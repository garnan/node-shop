const path = require('path');
const express = require('express');
const app = express();
const morgan = require('morgan');
const mongose = require('mongoose');

var multer = require('multer');

const storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null,'uploads/')
    },
    filename: (req,file,cb) => {
        cb(null,Date.now()+"-"+file.originalname)
    }
})
const upload = multer({storage});

const routes = require('./routes/index');

mongose.connect('mongodb://localhost/shop-test').then(db => console.log('db conected')).catch(err => console.log(err));

app.set('views', path.join(__dirname,'views'));
app.set('view engine','ejs');

app.use(morgan('dev'));
app.use(express.urlencoded({extended : false}));

app.use('/',routes);

app.listen(4000, () => {
    console.log('server bienvenido');
});