const express = require('express');
const rotuter = express.Router();

const Task = require('../models/task');
const User = require('../models/user');
const Producto = require('../models/producto');
const Carrito = require('../models/cart');
const Transaction = require('../models/transation');

rotuter.get('/',(req,res) => {
    res.render('index');
});

rotuter.post('/add', async (req,res) => {

    const task = new Task(req.body);

    await task.save();

    res.send(req.body);
});


rotuter.post('/validate', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const user = await User.find(req.body);

    res.render('validUser',{
        res,
        user
    })
    
});

rotuter.get('/user/:id',async (req,res) => {

    const user = await User.findById(req.params.id) ;

    const productos = await Producto.find();

    const cart = await Carrito.find({ id_user : req.params.id })

    const transation = await Transaction.find();

    res.render('user', {
        user,
        productos,
        cart,
        transation
    });
    
});

rotuter.get('/addAdmin', async (req,res) => {

    //{ name : "admin" , password : "123" , mail : "admin" , status : true  }

    var admin = { name : "admin" , password : "123" , mail : "admin" , status : true , tipo : "admin" };

    console.log(admin)

    const user = new User(admin);

    await user.save();

    res.send("administrador creado");
    
});


rotuter.post('/editPro', async (req,res) => {


        const user = await User.findById( req.body.id );

        console.log("usuario encontrado");
 
        user.name = req.body.name;
        user.mail = req.body.mail;
        user.password = req.body.password;

        await user.save();

        res.send("usuario cambiado");
    
});


//rest api

rotuter.get('/tasker', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const task = await Task.find();

    res.render('taskers',{
        task
    });
});


//datos de usuarios
rotuter.get('/selectUsers', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const users = await User.find();

    res.render('selectUsers',{
        users
    });
});

rotuter.post('/insertUser', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const user = new User(req.body);

    await user.save();

    console.log(req.body);

    res.send(req.body);
});


rotuter.get('/deleteUser/:id', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    var id = req.params.id;

    const user = await User.findByIdAndRemove(id)

    res.send(user);

});



//datos de productos


rotuter.get('/selectProductos', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const productos = await Producto.find();

    res.render('selectProductos',{
        productos
    });
});

rotuter.get('/selectProducto/:id', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    var id = req.params.id;

    const producto = await Producto.findById(id);

    res.send('{ "name" : "'+producto.name+'" , "price" : "'+producto.price+'"  }');
});

rotuter.post('/insertProducto', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const producto = new Producto(req.body);

    await producto.save();

    console.log(req.body);

    res.send(req.body);
});


rotuter.get('/deleteProducto/:id', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    var id = req.params.id;

    const producto = await Producto.findByIdAndRemove(id)

    res.send(producto);

});

//transacciones

rotuter.get('/selectTransactions', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const transacciones = await Transaction.find();

    res.render('selectTransactions',{
        transacciones
    });
});


rotuter.post('/addTransaction', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const transation = new Transaction(req.body);

    await transation.save();

    res.send(' { "result" : "accepted" } ')
});


//carrito
rotuter.get('/selectCarts/:id', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    var id = req.params.id;

    const cart = await Carrito.find( { id_usuario : id } );

    res.render('selectCarts',{
        cart
    });
});


rotuter.post('/findCarts', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const cart = await Carrito.find(req.body).count();

    res.send(' { "count" : "'+cart+'" } ')
});


rotuter.post('/addCarts', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    const cartcount = await Carrito.find(req.body).count();

    if(cartcount == 0){
        const cart = new Carrito(req.body);
        await cart.save();
    } else {
        //await Carrito.update( { id_usuario : req.body.id_usuario , id_producto : req.body.id_producto  } ,{  $set: req.body } );
        const cart = await Carrito.findOne( req.body );

        console.log("carrito encontrado");

        console.log(cart.count);

        cart.count = cart.count + 1;

        await cart.save();
    }

    res.send(req.body);

});


rotuter.post('/deleteCarts', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    await Carrito.remove(req.body); 

    res.send(req.body);

});

rotuter.post('/emptyCarts', async (req,res) => {

    res.setHeader('Content-Type', 'application/json');

    await Carrito.remove(req.body);

    res.send(req.body);

});







module.exports = rotuter;

