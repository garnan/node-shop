const mongose = require('mongoose');

const Schema = mongose.Schema;

const TaskSchema = new Schema({

    title : String,
    description : String,
    status : {
        type : Boolean,
        default: false
    }

});

module.exports = mongose.model('task',TaskSchema);
