const mongose = require('mongoose');

const Schema = mongose.Schema;

const UserSchema = new Schema({

    name : String,
    password : String,
    mail : String,
    tipo : String,
    status : {
        type : Boolean,
        default: true
    }

});

module.exports = mongose.model('user',UserSchema);
