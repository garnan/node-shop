const mongose = require('mongoose');

const Schema = mongose.Schema;

const TransactionSchema = new Schema({

    productos: String,
    total : Number,
    status : {
        type : Boolean,
        default: true
    }

});

module.exports = mongose.model('transaction',TransactionSchema);
