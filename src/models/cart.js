const mongose = require('mongoose');

const Schema = mongose.Schema;

const CartSchema = new Schema({

    id_usuario : String,
    id_producto : String,
    count : {
        type : Number,
        default : 1   
    },
    id_transaction : String,
    status : {
        type : Boolean,
        default: true
    }

});

module.exports = mongose.model('cart',CartSchema);
