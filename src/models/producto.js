const mongose = require('mongoose');

const Schema = mongose.Schema;

const ProductoSchema = new Schema({

    name : String,
    img : String,
    price : Number,
    status : {
        type : Boolean,
        default: true
    }

});

module.exports = mongose.model('producto',ProductoSchema);
